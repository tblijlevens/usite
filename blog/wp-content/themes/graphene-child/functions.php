<?
// Change some strings
function graphene_filter_readmore( $translated, $original, $domain ) {
	$strings = array(
                'Continue reading &raquo;' => 'Lees verder &raquo;',
		'Read the rest of this entry &raquo;' => 'Lees verder &raquo;',
		'Submit Comment' => 'Verzend',
		'Search' => 'Zoeken',
		'Go back to the front page' => 'Terug naar Home',
		'Reply' => 'Reageer',
		'1 comment' => '1 reactie',
		'comments' => 'reacties',
		'12 comments' => '12 reacties',
		'Edit comment' => 'Bewerk reactie',
		'Leave comment' => 'Laat een reactie achter',
		'Leave a Reply' => 'Laat een reactie achter',
		'About the author' => 'Over de auteur',
		'Message:' => 'Bericht:',
		'Your email address will not be published.' => 'Reacties met 1 of meer links moeten eerst worden goedgekeurd door de administrator (i.v.m. spam)',
		'Name:' => 'Naam:',
		'Email:' => 'E-mail: (wordt niet gepubliceerd)',
		'Website:' => 'Website: (wordt niet gepubliceerd)',
		'Cancel reply' => 'Annuleer de reactie',
		'Anonymous' => 'Anoniem',

	);
	if ( ! empty( $strings[$original] ) ) {
		$translations = &get_translations_for_domain( $domain );
		$translated = $translations->translate( $strings[$original] );
	}
	return $translated;
}
add_filter( 'gettext', 'graphene_filter_readmore', 10, 3 );
?>
