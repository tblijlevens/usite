
// Scripted By Adam Khoury in connection with the following video tutorial:
// http://www.youtube.com/watch?v=c_ohDPWmsM0


/* see the memory_array below. All pictures should name the category in the
first 5 characters of the picture-name (e.g: cloth_muslima2_pixabay. 'cloth_'
being the category). Categories can be named anything (also abbreviations),
as long as it's exactly the same as the other pictures in the same category
and unique from the other categories and 5 characters in length. All actual
pictures should all be in the same folder: images. All picture filename
extentions should have the '.jpg' extention. Convert any .png or other formats
into jpg for this code to work. 'jpeg' can just be renamed to jpg.
*/
var memory_array = [
'bake_Doughnut',
'bake_Doughnut2',
'bake_Doughnut3',
'food_Burger',
'food_Meal',
'food_Schnitzel',
'girl_Western_woman',
'girl_Western_woman2',
'girl_Western_woman3',
'hopr_Church',    //abbreviation for House Of PRayer
'hopr_Church2',
'hopr_Church3',
'land_Landscape',
'land_Landscape2',
'land_snow',
'scrp_Mind',
'scrp_Remembrance',
'scrp_MemoryW',
'vill_Western_village',
'vill_Western_village2',
'vill_Western_village3',
'wedd_Western_wedding',
'wedd_Western_wedding2',
'wedd_Western_wedding3'
];
/* make sure the amount of pictures in the memory_array is a multitude of 3!
Otherwise your player can not finish the board. E
very name in the array should be between apostrophes (').
Every name in the array should end with a comma (,) except the last one (to seperate the different entries).
*/

var memory_values = [];       //this array will eventually hold the categories of the flipped cards
var memory_tile_ids = [];
var tiles_flipped = 0;
var begintime;
var endtime;
var duration;
var numtries = 0;
var cardsflipped = 0;

Array.prototype.memory_tile_shuffle = function(){
    var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
}

function newBoard(){
  document.getElementById('memory_board').innerHTML = "";
  $("#endscore").fadeOut(500);
  tiles_flipped = 0;
  number=0;
	var output = '';
    memory_array.memory_tile_shuffle();
	for(var i = 0; i < memory_array.length; i++){
		output += '<div id="tile_'+i+'" onclick="memoryFlipTile(this,'+ i +',\''+ memory_array[i].slice(0, 5) +'\')"></div>';
	}
	document.getElementById('memory_board').innerHTML = output;
}

// each time a card is clicked the following function is called
function memoryFlipTile(tile,i, category){
  if (number==1){
  begintime = new Date().getTime() / 1000;    //current time in seconds since 1970 is stored in variable begintime (only if it is the first card that is flipped)
};
  number++;
  if (memory_tile_ids.indexOf(tile.id)==-1){cardsflipped++}; //variable 'cardsflipped' will add 1 to itself (if the card was not already open)
	if(tile.innerHTML == "" && memory_values.length < 3){
		tile.style.background = '#FFF';
		tile.innerHTML = '<img src="images/' + memory_array[i] + '.jpg" width="100%"/>';    //the 'flipped' card will show the right picture
		if(memory_values.length == 0){
			memory_values.push(category);
			memory_tile_ids.push(tile.id);
		} else if(memory_values.length == 1){
			memory_values.push(category);
			memory_tile_ids.push(tile.id);
    } else if(memory_values.length == 2){ //if this is the last card (of a set of three) that has been clicked
			memory_values.push(category);
			memory_tile_ids.push(tile.id);
			if(memory_values[0] == memory_values[1] && memory_values[0] == memory_values[2]){ //if the three open cards belong to the same category
        numtries++;   //variable numtries will add 1 to itself
        $("#compliment").fadeIn(500).fadeOut(500);  //the compliment will fade in and out the number is the time it will take in milliseconds (e.g. 500 is 0.5 seconds)
				tiles_flipped += 3;   //variable will add 3 to itself
				// Clear both arrays
				memory_values = [];
        memory_tile_ids = [];
				if(tiles_flipped == memory_array.length){  //if al the cards are flipped
          endtime = new Date().getTime() / 1000;  //current time will be stored in endtime
          duration = Math.round((endtime-begintime) * 100) / 100; //duration = endtime-begintime
          $("#boxscore").html('<B>Score</B><br><br>Time played: ' + duration + ' seconds<br>Number of tries: ' + numtries + '<br>Number of cards flipped: ' + cardsflipped + '<br><br><a href="#top" onclick="newBoard()">Again</a>');   // text of the endscore updates (<br>=newline, inside apostrophes is static text, outside apostrophes (e.g. duration) are variables)
          $("#endscore").fadeIn(500);  //the endscore will fade in
				}
			} else {   // if the three open cards do not belong to the same category
				function flip2Back(){
				    // Flip the 3 tiles back over
            numtries++;
				    var tile_1 = document.getElementById(memory_tile_ids[0]);
				    var tile_2 = document.getElementById(memory_tile_ids[1]);
            var tile_3 = document.getElementById(memory_tile_ids[2]);
				    tile_1.style.background = '#000099';    //this color should be the same as the background in your stylesheet at #memory_board > div
            	    tile_1.innerHTML = "";
				    tile_2.style.background = '#000099';
            	    tile_2.innerHTML = "";
            tile_3.style.background = '#000099';
            	    tile_3.innerHTML = "";
				    // Clear both arrays
				    memory_values = [];
            memory_tile_ids = [];
				}
				setTimeout(flip2Back, 700);   //calling the function to flip back the cards is done after a timeout. change the number (milliseconds) to let the player view the cards longer or shorter before they are turned
			}
		}
	}
}
