$( document ).ready(function() {
    $(document).keydown(function(key) {
        switch(parseInt(key.which,10)) {
			// Left arrow key pressed
			case 37:
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({left: '-=10px'}, 100);
				$('p').html('Left!');
				$('p').fadeOut(1000);
				break;
			// Up Arrow Pressed
			case 38:
				// Put our code here
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({top: '-=10px'}, 100);
				$('p').html('Up!');
				$('p').fadeOut(1000);
				break;
			// Right Arrow Pressed
			case 39:
				// Put our code here
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({left: '+=10px'}, 100);
				$('p').html('Right!');
				$('p').fadeOut(1000);
				break;
			// Down Arrow Pressed
			case 40:
				// Put our code here 
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({top: '+=10px'}, 100);
				$('p').html('Down!');
				$('p').fadeOut(1000);			
				break;
			// Spacebar Pressed
			case 32:
				// Put our code here
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({top: '+=100px', left: '+=50'}, 200);
				$('div').animate({left: '+=20'}, 50);				
				$('div').animate({top: '-=100px', left: '+=50'}, 200);
				$('p').html('Jump!... down?');
				$('p').fadeOut(1000);
				break;

			// R Pressed
			case 82:
				// Put our code here
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({left: '+=300'}, 200);				
				$('p').html('Dash right!');
				$('p').fadeOut(1000);
				break;
				
			// E Pressed
			case 69:
				// Put our code here
				$('p').html();
				$('p').fadeIn(0);
				$('div').animate({left: '-=300'}, 200);				
				$('p').html('Dash left!');
				$('p').fadeOut(1000);
				break;				
		}
	});
});