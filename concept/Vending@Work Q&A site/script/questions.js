//// object met alle questions
var questions = {
  numberOfQuestions:0,

  //// algemeen questions
  question1 : {
    catagory: 'Algemeen',
    html: '<div id="question1" class="question"><img src="images/right-icon small.png" height="20px">Kan je met meerdere mensen tegelijkertijd in het systeem werken?<div class="answer"><br>Ja</div></div><br><br><hr>',
    open: "no"
  },
  question2 : {
    catagory: 'Algemeen',
    html: '<div id="question2" class="question"><img src="images/right-icon small.png" height="20px">Worden wijzigingen aan de gegevens van een klant (bijvoorbeeld adresgegevens) direct ge&#252;pdatet bij alle automaten van die klant?<div class="answer"><br>Ja</div></div><br><br><hr>',
    open: "no"
  },

  //// operator questions
  question3 : {
    catagory: 'Operator',
    html: '<div id="question3" class="question"><img src="images/right-icon small.png" height="20px">Hoe weet een operator welke en hoeveel producten hij moet vullen als hij bij een klant aankomt?<div class="answer"><br>De operator vult in hoeveel producten hij per spiraal vult. De app geeft de gemiddelde vulaantallen weer van de afgelopen 6 weken. De operator hoeft nu niet meer eerst langs de machine om te kijken wat er in moet, om vervolgens die spullen te halen en weer te gaan vullen. Hij kan in &#233;&#233;n keer alle spullen meenemen en hoeft zo maar 1 keer heen en weer.</div></div><br><br><hr>',
    open: "no"
  },
  question4 : {
    catagory: 'Operator',
    html: '<div id="question4" class="question"><img src="images/right-icon small.png" height="20px">Als een route wordt gereden met 2 operators, hoe werkt dat dan met de smartphone?<div class="answer"><br>Als beide operators tegelijkertijd 2 machines gaan vullen (en vulaantallen invullen) dan hebben beide operators een eigen smartphone nodig met dezelfde route er op.</div></div><br><br><hr>',
    open: "no"
  },

  //// indeling questions
  question5 : {
    catagory: 'Indelingen',
    html: "<div id='question5' class='question'><img src='images/right-icon small.png' height='20px'>Kan de kantoormedewerker in alle planograms van alle machines in &#233;&#233;n keer een product vervangen met een nieuw product? Bijvoorbeeld: alle eat natural bars moeten kit kat chunky's worden in alle planograms.<div class='answer'><br>Ja</div></div><br><br><hr>",
    open: "no"
  },
  question6 : {
    catagory: 'Indelingen',
    html: "<div id='question6' class='question'><img src='images/right-icon small.png' height='20px'>Of in een selectie van machines, bijvoorbeeld alleen RUG automaten, alleen klanten in het Noorden, alleen middelbare scholen, etc?<div class='answer'><br>Ja. Machines kunnen gecategoriseerd worden (bijvoorbeeld middelbare scholen, enz.). Veranderingen in machines (producten in indelingen, prijzen, etc.) kan je doen: <ul><li>per machine</li><li>voor alle machines in een categorie</li><li>voor alle machines</li></ul></div></div><br><br><hr>",
    open: "no"
  },

  //// planning questions
  question7 : {
    catagory: 'Plannen',
    html: "<div id='question7' class='question'><img src='images/right-icon small.png' height='20px'>Kan je in &eacute;&eacute;n keer meerdere machines selecteren en toevoegen aan een route? Of moet het &eacute;&eacute;n voor &eacute;&eacute;n?<div class='answer'><br>Ja meerdere tegelijk.</div></div><br><br><hr>",
    open: "no"
  },
  question8 : {
    catagory: 'Plannen',
    html: "<div id='question8' class='question'><img src='images/right-icon small.png' height='20px'>Kan je makkelijk de volgorde van machines op een route veranderen? Slepen?<div class='answer'><br>Ja, slepen.</div></div><br><br><hr>",
    open: "no"
  },
  question9 : {
    catagory: 'Plannen',
    html: "<div id='question9' class='question'><img src='images/right-icon small.png' height='20px'>Hoe verplaats je een hele route van de ene dag naar de andere dag? Kan je makkelijk in een keer alle machines van een route naar een andere dag verplaatsen of moet je elke machine apart verplaatsen?<div class='answer'><br>Je sleept de routes/machines gewoon naar een andere dag</div></div><br><br><hr>",
    open: "no"
  },
  question10 : {
    catagory: 'Plannen',
    html: "<div id='question10' class='question'><img src='images/right-icon small.png' height='20px'>Kan je makkelijk een andere koerier toewijzen aan een route?<div class='answer'><br>Ja, een koerier heeft zijn eigen route toegewezen. Als de koerier ziek is, kan de vervanger inloggen op de account van de vervangde koerier of de route over laten plaatsen naar zijn eigen account.</div></div><br><br><hr>",
    open: "no"
  },
  question11 : {
    catagory: 'Plannen',
    html: "<div id='question11' class='question'><img src='images/right-icon small.png' height='20px'>Kan je &eacute;&eacute;n automaat aan meerdere routes toevoegen/inplannen? Als een automaat al op de route van dinsdag staat, kan ik hem dan ook alvast inplannen op donderdag, of moet ik wachten tot de route is gereden voordat ik de automaat opnieuw kan inplannen?<div class='answer'><br>Ja, je kan automaten op meerdere momenten inplannen.</div></div><br><br><hr>",
    open: "no"
  },
  question12 : {
    catagory: 'Plannen',
    html: "<div id='question12' class='question'><img src='images/right-icon small.png' height='20px'>Hoe weet je dat een machine ingepland moet worden?<div class='answer'><br>Doordat de operator vulaantallen per spiraal invult, wordt veel accurater bijgehouden hoe snel een machine loopt en hoe vaak deze ingepland moet worden. Automaten met telemetrie erin houden bij hoeveel er verkocht is. Verwachting is dat steeds meer automaten deze telemetrie gaan krijgen (i.v.m. de groei van cashless betalen waar telemetrie aan gekoppeld is). Zo kan je live zien hoe vol de automaat staat en of deze ingepland moet worden, mits de koerier goed heeft ingevuld hoe vol hij de automaat heeft gezet. De planner kan ook rapporten opvragen van routes, waarmee aan de hand van de omzet, de frequentie en meer parameters bepaald kan worden of een automaat of een groep automaten ingepland moet worden. Verder kan de planner notifications krijgen voor bijv: <ul><li>deze automaat moet eens in de twee weken, dus hij moet deze week</li><li>Automaat staat x % leeg (als telemetrie is ingesteld)</li></ul></div></div><br><br><hr>",
    open: "no"
  },
  question13 : {
    catagory: 'Plannen',
    html: "<div id='question13' class='question'><img src='images/right-icon small.png' height='20px'>Worden sommige automaten door het systeem automatisch ingepland op een route?<div class='answer'><br>Ja, dat is mogelijk. Het systeem kan automatisch automaten inplannen, maar dit is wel bedoeld als 'plan-hulp'. Aandacht en overzicht van en door de planner is nog steeds van belang om optimale planningen te maken. Men moet er niet van uit gaan dat het systeem alles perfect inplant, maar wel werk kan wegnemen door goede suggesties te maken, zodat de planner minder uitzoek-werk heeft.</div></div><br><br><hr>",
    open: "no"
  },
  question14 : {
    catagory: 'Plannen',
    html: "<div id='question14' class='question'><img src='images/right-icon small.png' height='20px'>Kan het systeem automatisch bepalen welke volgorde het handigst is dat de operator langs de klanten rijdt?<div class='answer'><br>Ja, het systeem kan geografisch de meest effici&euml;nte volgorde bepalen. Ook kan je daarmee het systeem rekening laten houden met openingstijden (per automaat kan je de openingstijden instellen). Elke automaat krijgt een gemiddelde operating tijd, waardoor het ongeveer kan incalculeren hoe lang de operator bezig is per automaat en dus hoe laat hij ongeveer waar aankomt. </div></div><br><br><hr>",
    open: "no"
  },
  question15 : {
    catagory: 'Plannen',
    html: "<div id='question15' class='question'><img src='images/right-icon small.png' height='20px'>Bij het aanmaken van een planning voor een route kan je momenteel niet tussendoor iets anders binnen het systeem doen, want dan wordt de route niet opgeslagen. Kan dit in het nieuwe systeem wel?<div class='answer'><br>Ja, je kan de route op elk moment opslaan en later afmaken.</div></div><br><br><hr>",
    open: "no"
  },
  question16 : {
    catagory: 'Plannen',
    html: "<div id='question16' class='question'><img src='images/right-icon small.png' height='20px'>Kan je een opmerking voor de operator plaatsen bij een specifieke machine? Bijvoorbeeld een verzoek om van deze machine een foto te maken? Of om geld terug te geven aan de receptie?<div class='answer'><br>Ja.</div></div><br><br><hr>",
    open: "no"
  },

  //// TD questions
  question17 : {
    catagory: 'TD',
    html: "<div id='question17' class='question'><img src='images/right-icon small.png' height='20px'>Hoe wordt een storing verwerkt? Een klant belt over een storing aan een automaat, wat gebeurt er vervolgens?<div class='answer'><br>Degene die de storing aanneemt zet deze direct in het systeem bij de juiste automaat en markeert deze als storing. Ook plant hij/zij direct de automaat in bij de juiste operator of bij de TD.</div></div><br><br><hr>",
    open: "no"
  },

  //// Magazijn questions
  question18 : {
    catagory: 'Magazijn',
    html: "<div id='question18' class='question'><img src='images/right-icon small.png' height='20px'>Hoe weet de magazijnmedewerker wat hij moet klaarleggen voor een bepaalde route?<div class='answer'><br>Het systeem kan automatisch paklijsten genereren op basis van welke automaten er op de route staan en de gemiddelde vulaantallen van die automaten.</div></div><br><br><hr>",
    open: "no"
  },

  //// Verwerken questions
  question19 : {
    catagory: 'Verwerken',
    html: "<div id='question19' class='question'><img src='images/right-icon small.png' height='20px'>Kan ik makkelijk alle notities bekijken die een operator heeft gemaakt op zijn route gisteren?<div class='answer'><br>Ja. Je kan van elke route en elke datum een rapport opvragen van de gemaakte notities.</div></div><br><br><hr>",
    open: "no"
  },
};


function updateNumberOfQuestions(){
  for (question in questions) {
    if (questions[question].open === "no"){
      questions.numberOfQuestions++;
    }
  }
}
