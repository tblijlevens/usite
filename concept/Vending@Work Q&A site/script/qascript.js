$(document).ready(function(){

updateNumberOfQuestions();

//// deze forloop bouwt de hele html op
for (var i = 1; i <= questions.numberOfQuestions; i++) {
	if (questions["question"+i]["catagory"] == "Algemeen"){
		$("#algemeen").append(questions["question"+i]["html"]);
	}
	else if (questions["question"+i]["catagory"] == "Operator") {
		$("#operator").append(questions["question"+i]["html"]);
	}
	else if (questions["question"+i]["catagory"] == "Indelingen") {
		$("#indelingen").append(questions["question"+i]["html"]);
	}
	else if (questions["question"+i]["catagory"] == "Plannen") {
		$("#plannen").append(questions["question"+i]["html"]);
	}
	else if (questions["question"+i]["catagory"] == "TD") {
		$("#technischedienst").append(questions["question"+i]["html"]);
	}
	else if (questions["question"+i]["catagory"] == "Magazijn") {
		$("#magazijn").append(questions["question"+i]["html"]);
	}
	else if (questions["question"+i]["catagory"] == "Verwerken") {
		$("#verwerken").append(questions["question"+i]["html"]);
	}
}

///// deze functie schuift het antwoord uit zodra er op een vraag wordt geklikt
$(".answer").hide();
var open = "no";
var whatWasClicked;
var numberOfWhatWasClicked;

$(".question").click(function(){
			whatWasClicked = event.target.id;
	if (questions[whatWasClicked]["open"] == "no"){
		$("#"+whatWasClicked + "> :nth-child(2)").show(300);
		questions[whatWasClicked]["open"] = "yes";
		$("#"+whatWasClicked + "> :nth-child(1)").attr('src','images/down-icon small.png');
	}
	else {
		$("#"+whatWasClicked + "> :nth-child(2)").hide(300);
		questions[whatWasClicked]["open"] = "no";
		$("#"+whatWasClicked + "> :nth-child(1)").attr('src','images/right-icon small.png');
	}
})

$("#verstuur").click(function(){
	alert("Bedankt voor je vraag. Zodra er een antwoord is gevonden zal deze op de site worden geplaatst en krijg je daar een mailtje over.");
	window.location.href='index.html';
})

});
