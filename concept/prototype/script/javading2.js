/* 
Author & productowner: Teun Blijlevens, Umanise.
Date: 01 October 2015 
Use of this product is only allowed  if permission  is given by the productowner.
Contact: mail@umanise.nl  
*/

$(document).ready(function(){

		$('#home').hide(0);
		$('#home2').hide(0);
		$('#harmonie').hide(0);
		$('#notepage').hide(0);		
		$('#planogram').hide(0);
		$('#planogram2').hide(0);
		$('#cashencounts').hide(0);
		$('#clientinfo').hide(0);
		$('#vulaantalnavi').hide(0);
/////////////////////////////////////////
////			VARIABLES			/////
/////////////////////////////////////////
var messageRead = 0;

var doneCLeft = 1;

var doneCRight = 1;
var Solved = 1;
var numberOfNotes = 2;

var whichPlano='<img src="images/harmonieplanoleftrood.png" width="100%">';
var whichDrawer=0;
var drawer1BigSmall= 'small';
var drawer2BigSmall= 'small';

var currentScreen = "start";
var screenArray = ["start"];



flashingThing(); 

	function flashingThing(){
	if (Solved % 2 != 0){
	$('#hamer').animate({width:'42px', height:'36px', left: '-=7px', top: '-=5.5px'}, 500);
	$('#hamer').animate({width:'28px', height:'25px', left: '+=7px', top: '+=5.5px'}, 500);
	$('#hamer').animate({width:'42px', height:'36px', left: '-=7px', top: '-=5.5px'}, 500);
	$('#hamer').animate({width:'28px', height:'25px', left: '+=7px', top: '+=5.5px'}, 500);
	$('#hamer').animate({width:'42px', height:'36px', left: '-=7px', top: '-=5.5px'}, 500);
	$('#hamer').animate({width:'28px', height:'25px', left: '+=7px', top: '+=5.5px'}, 500);
};

if (messageRead == 0){
	$('#message').animate({width:'36px', left: '-=7px', top: '-=5.5px'}, 500);
	$('#message').animate({width:'22px', left: '+=7px', top: '+=5.5px'}, 500);
	$('#message').animate({width:'36px', left: '-=7px', top: '-=5.5px'}, 500);
	$('#message').animate({width:'22px', left: '+=7px', top: '+=5.5px'}, 500);
	$('#message').animate({width:'36px', left: '-=7px', top: '-=5.5px'}, 500);
	$('#message').animate({width:'22px', left: '+=7px', top: '+=5.5px'}, 500);
}
else if (numberOfNotes != 0){
	$('#message').css('top', '4px');
	$('#message').html('<img src="images/envelopjeopen.png" width="100%">');
}
else {
	$('#message').html('');
	$('#message2').fadeOut(0);
};
};


/////////////////////////////////////////
////	FUNCTIES OP ALLE PAGINA'S	/////
/////////////////////////////////////////



	function E3Liveclick(){
		setTimeout(function(){
			$('#home').fadeOut(0);
			$('#home2').fadeOut(0);
			$('#jobs').fadeIn(0);
			$('#materialen').fadeIn(0);
			$('#harmonie').fadeOut(0);
			$('#notepage').fadeOut(0);		
			$('#planogram').fadeOut(0);
			$('#planogram2').fadeOut(0);
			$('#vulaantalnavi').fadeOut(0);
			$('#cashencounts').fadeOut(0);
			$('#clientinfo').fadeOut(0);
			$('#PhoneImg').html('<img src="images/startscherm.jpg" width="100%">');
			currentScreen="start";
			if (screenArray[screenArray.length-1] != "start") {
				screenArray.push(currentScreen);
			};
			$('#testdiv').html('Page 1: Start screen	<div id="testdivtext">Extra materials are available from the start screen. <BR><BR>Shortcuts to call the office and technical service are added, so the operator doesn&#39;t have to leave the app <BR><BR>Refreshing all the data of the route is made easy by making the refresh button available on the start screen.<BR><BR>The E3Live logo at the top always redirects to the start screen troughout the app.<BR><BR><BR><BR><i>Only the top two buttons work in this demo.</i></div>')
		},400);
		
		
	};
	
$('#E3Live').click(function() {
	
	
	$('#E3Live').animate({opacity: 1, width: 92, height: 52, left: "-=7.5", top: "-=4"}, 300);
	$('#E3Live').animate({opacity: 0, width: 77, height: 44, left: "+=7.5", top: "+=4"}, 200);
	E3Liveclick();
});		


$('#Back').click(function() {
	$('#Back').animate({opacity: 1, width: 86, height: 54, left: "-=8", top: "-=5"}, 300);
	$('#Back').animate({opacity: 0, width: 70, height: 44, left: "+=8", top: "+=5"}, 200);
	 screenArray.splice(screenArray.length-1, 1);
	 
	 if (screenArray[screenArray.length-1] == "start"){
		 E3Liveclick();
	 };
	 if (screenArray[screenArray.length-1] == "jobs"){
		 jobsclick();
	 };
	 if (screenArray[screenArray.length-1] == "home"){
		 harmonieclick();
	 };
	 if (screenArray[screenArray.length-1] == "cashencounts"){
		 cashencountsclick();
	 };
	 	 if (screenArray[screenArray.length-1] == "planogram"){
		 planogramclick();
	 };
	 
	if (currentScreen == "notepage"){
		$('#notepage').fadeOut(500);
		harmonieclick(); 
	};
});


/////////////////////////////////////////
////	STARTPAGINA FUNCTIES		/////
/////////////////////////////////////////



	function jobsclick(){

		setTimeout(function(){
			$('#home').fadeOut(0);
			$('#home2').fadeOut(0);
			$('#jobs').fadeOut(0);
			$('#materialen').fadeOut(0);
			$('#harmonie').fadeIn(0);
			$('#notepage').fadeOut(0);		
			$('#planogram').fadeOut(0);
			$('#planogram2').fadeOut(0);
			$('#vulaantalnavi').fadeOut(0);
			$('#cashencounts').fadeOut(0);
			$('#clientinfo').fadeOut(0);
			$('#PhoneImg').html('<img src="images/jobs.jpg" width=100%">');
			currentScreen="jobs";
			if (screenArray[screenArray.length-1] != "jobs") {
				screenArray.push(currentScreen);
			};
			$('#testdiv').html('Page 3: Jobs screen	<div id="testdivtext">Machines are grouped per client/location. <br><br>Icons show the types of jobs per location, including service jobs, which are now integrated with &#39;normal&#39; jobs. <br><br>Each client shows the number of machines that need attention. <BR><BR>Each client shows the city and streetname. <BR><BR><BR><BR><I>In this demo version only the top client button works.</I></div>')
		},400);
	};

$('#jobs').click(function() {

	$('#jobs').animate({opacity: 1, width: 397, height: 128, left: "-=33", top: "-=10.5"}, 300);
	$('#jobs').animate({opacity: 0, width: 331, height: 107, left: "+=33", top: "+=10.5"}, 200);
	
	jobsclick();

});		


	function materialenclick(){
		setTimeout(function(){
			$('#home').fadeOut(0);
			$('#home2').fadeOut(0);
			$('#jobs').fadeOut(0);
			$('#materialen').fadeOut(0);
			$('#harmonie').fadeOut(0);
			$('#notepage').fadeOut(0);		
			$('#planogram').fadeOut(0);
			$('#planogram2').fadeOut(0);
			$('#vulaantalnavi').fadeOut(0);
			$('#cashencounts').fadeOut(0);
			$('#clientinfo').fadeOut(0);
			$('#PhoneImg').html('<img src="images/materialen.jpg" width=100%">');
			currentScreen="materialen";
			if (screenArray[screenArray.length-1] != "materialen") {
				screenArray.push(currentScreen);
			};
			$('#testdiv').html('Page 2: Materials screen	<div id="testdivtext">Information for this screen is collected from the materials fields in the &#39;Client info&#39; tab of each machine on today&#39;s route.</div>')
		},400);
	};		
	
$('#materialen').click(function() {
	
	$('#materialen').animate({opacity: 1, width: 397, height: 128, left: "-=33", top: "-=10.5"}, 300);
	$('#materialen').animate({opacity: 0, width: 331, height: 107, left: "+=33", top: "+=10.5"}, 200);
	
	materialenclick();
});		


/////////////////////////////////////////
////	JOBS PAGINA FUNCTIES		/////
/////////////////////////////////////////



	function harmonieclick(){
		setTimeout(function(){
			$('#PhoneImg').html('<img src="images/harmoniehomeleeg.jpg" width=100%">');
					$('#jobs').fadeOut(0);
					$('#materialen').fadeOut(0);
					$('#harmonie').fadeOut(0);
					$('#home').fadeIn(0);
					$('#home2').fadeIn(0);
					$('#cashencounts').fadeIn(0);
					$('#planogram').fadeIn(0);
					$('#planogram2').fadeOut(0);
					$('#clientinfo').fadeIn(0);
					$('#notepage').fadeOut(0);		
					$('#vulaantalnavi').fadeOut(0);
					flashingThing();
					currentScreen="home";
					if (screenArray[screenArray.length-1] != "home") {
						screenArray.push(currentScreen);
					};
					$('#testdiv').html('Page 4: Client home	<div id="testdivtext">Enable a task oriented approach by grouping machines and using tabs in the interface. <br><br>Each machine is coloured corresponding to the task. <BR><font color="#01ffff">Blue</font>: machine needs filling and cashing <BR><font color="#ff7777">Red</font>: machine needs servicing <BR><font color="#9eff9e"> Green</font>: machine is done.<br><br>Ticking a box will change the colour of the machine. <i>&#39;TD&#39; is not tickable in this demo.</i><BR><BR>The &#39;Done&#39; tick box of a machine that needs servicing can only be ticked if either &#39;Solved&#39; (the operator fixed the problem) or &#39;TD&#39; (the operator can&#39;t fix it and calls in Technical Service) is already ticked.<BR><BR>Each machine has two fields: an unadjustable field with remarks from the office or technical service and a field where the operator can type a remark.<BR><BR>If the last operator that visited this machine left remarks, a clickable red envelope icon appears, see candy left. If the notes are read, the envelope is black and open. If all notes are deleted, the icon disappears.</div>');
		},400);
	};		
	
$('#harmonie').click(function() {
	
	$('#harmonie').animate({opacity: 1, width: 397, height: 108, left: "-=33", top: "-=9"}, 300);
	$('#harmonie').animate({opacity: 0, width: 331, height: 90, left: "+=33", top: "+=9"}, 200);
	
	harmonieclick();
});	

/////////////////////////////////////////
////	harmonie HOME FUNCTIES		/////
/////////////////////////////////////////


	
$('#home').click(function() {
	
	$('#home').animate({opacity: 1, width: 66, height: 36, left: "-=5.5", top: "-=3"}, 300);
	$('#home').animate({opacity: 0, width: 55, height: 30, left: "+=5.5", top: "+=3"}, 200);
	harmonieclick();
});		

	function cashencountsclick(){
		setTimeout(function(){
			$('#home2').fadeOut(0);
			$('#notepage').fadeOut(0);
			$('#planogram2').fadeOut(0);
			$('#PhoneImg').html('<img src="images/cash en counts.jpg" width=100%">');
			currentScreen="cashencounts";
			if (screenArray[screenArray.length-1] != "cashencounts") {
				screenArray.push(currentScreen);
			};
			$('#testdiv').html('Page 6: Cash and counts	<div id="testdivtext">Grouping machines this way gives a lot of flexibility. An operator can decide for himself in which order he wishes to perform the tasks, instead of the app determining it for him. This way both task oriented and machine oriented approaches are possible:<BR><ul><li>fill in all turnover counts of all machines and then scan all money bags of all machines (task oriented approach) </li><li>scan the money bag and register the turnover count of one machine and then do the same with the next machine (machine oriented approach)</<li></<ul></div>');
		},400);
	};
	
$('#cashencounts').click(function() {
	
	$('#cashencounts').animate({opacity: 1, width: 66, height: 36, left: "-=5.5", top: "-=3"}, 300);
	$('#cashencounts').animate({opacity: 0, width: 55, height: 30, left: "+=5.5", top: "+=3"}, 200);
	cashencountsclick();
});		



	function planogramclick(){
		setTimeout(function(){
			$('#home2').fadeOut(0);
			$('#notepage').fadeOut(0);
			$('#planogram2').fadeIn(0)
			$('#PhoneImg').html(whichPlano);
			$("#twoplanograms").show(0);
			$("#draggable").draggable({ axis: "x", containment: "#containment-wrapper", scroll: false,  
					drag: function(){
						var offset = $(this).offset();
						var xPos = offset.left;
						if (xPos < 400){
							whichPlano='<img src="images/harmonieplanorightrood.png" width="100%">';
							$('#PhoneImg').html(whichPlano);
							
						};
						if (xPos > 400){
							whichPlano='<img src="images/harmonieplanoleftrood.png" width="100%">';
							$('#PhoneImg').html(whichPlano);

						};
					},
					
			});
			currentScreen="planogram";
			if (screenArray[screenArray.length-1] != "planogram") {
				screenArray.push(currentScreen);
			};
			$('#testdiv').html('Page 7: Planograms 	<div id="testdivtext">The main planogram is in a visual format, making it very easy for the operator to see which product goes in which spiral at a glance. <BR><BR>The listed planogram is below the graphical one, with only the relevant information for the operator: spiral number, product, price.<BR><BR>Clicking the buttons will navigate to the selected machine&#39;s planogram. <i>Only the top two buttons work in this demo</i>. Horizontally swiping the planogram will also switch between machines. <BR><BR>Each drawer in the visual planogram is clickable, making the filling count input fields of the spirals in that drawer appear. <i>Only the top two drawers work in this demo.</i><BR>This too, makes the work process of the operator more flexible. The current app urges the operator to fill the machine in the order of the listed planogram, making him need to unstack his cart to get to the right products, creating chaos in the process and needing to find put-away products later on. By enabling visual planograms with clickable drawers, the operator can now easily choose the order in which he wants to fill the machine, usually depending on which products are on the top of his cart. </div>');
			
		},400);
	};
	
	
$('#planogram').click(function() {
	
	$('#planogram').animate({opacity: 1, width: 66, height: 36, left: "-=5.5", top: "-=3"}, 300);
	$('#planogram').animate({opacity: 0, width: 55, height: 30, left: "+=5.5", top: "+=3"}, 200);
	planogramclick();
});		



	function clientinfoclick(){
		setTimeout(function(){
			$('#home2').fadeOut(0);
			$('#notepage').fadeOut(0);
			$('#planogram2').fadeOut(0);
			$('#PhoneImg').html('<img src="images/clientinfo.jpg" width=100%">');
			currentScreen="clientinfo";
			if (screenArray[screenArray.length-1] != "clientinfo") {
				screenArray.push(currentScreen);
			};
			$('#testdiv').html('Page 8: Client info	<div id="testdivtext">This screen shows additional information about the client and the machines. An operator can check this if he needs directions to the machines or when he doesn&#39;t know which keys to use. <BR><BR>The information in the three materials fields (keys, service materials, other materials) are collected and shown on the &#39;materials&#39; screen, available from the start screen. <BR><BR>The start screen of a machine used in the current app is now obsolete.</div>');
		},400);
	};
	
$('#clientinfo').click(function() {

	$('#clientinfo').animate({opacity: 1, width: 66, height: 36, left: "-=5.5", top: "-=3"}, 300);
	$('#clientinfo').animate({opacity: 0, width: 55, height: 30, left: "+=5.5", top: "+=3"}, 200);
	clientinfoclick();
});		


////////////		harmonieHOME VINKJES E.D.		/////////

$('#donevinkcleft').click(function() {
    doneCLeft++
	$('#donevinkcleft').animate({opacity: 1, width: 16, height: 16, left: "-=2", top: "-=2"}, 300);
	$('#donevinkcleft').animate({opacity: 0, width: 12, height: 12, left: "+=2", top: "+=2"}, 200);
	if (doneCLeft % 2 == 0){
		$('#cleftimg').replaceWith('<img id="cleftimg" src="images/CanteenCandyLeftGreen.jpg" width="100%">')
	}
	else {
		$('#cleftimg').replaceWith('<img id="cleftimg" src="images/CanteenCandyLeftBlue.jpg" width="100%">')
	};
});		


$('#message2').click(function() {
    messageRead=1;
	currentScreen = "notepage";
	screenArray.push(currentScreen);
	$('#message2').animate({opacity: 1, width: 30, height: 30, left: "-=4", top: "-=4"}, 300);
	$('#message2').animate({opacity: 0, width: 22, height: 22, left: "+=4", top: "+=4"}, 200);

	
	$('#notepage').delay(200).fadeIn(500);
	
	$('#testdiv').html('Page 5: Previous operator notes	<div id="testdivtext">A note will stay visible for next visits until an operator deletes the note.</div>');
});

$('#delete1').click(function() {
	numberOfNotes--;
	$('#delete1').animate({opacity: 1, width: 129, height: 40, left: "-=11", top: "-=3.5"}, 300);
	$('#delete1').animate({opacity: 0, width: 107, height: 33, left: "+=11", top: "+=3.5"}, 200);
	
	$('#notekees').fadeOut(500);
	$('#notejustus').animate({top: 45}, 1000);

});

$('#delete2').click(function() {
	numberOfNotes--;
	$('#delete2').animate({opacity: 1, width: 129, height: 40, left: "-=11", top: "-=3.5"}, 300);
	$('#delete2').animate({opacity: 0, width: 107, height: 33, left: "+=11", top: "+=3.5"}, 200);
	
	$('#notejustus').fadeOut(500);

});

$('#solvedvink').click(function() {
	Solved++
    $('#solvedvink').animate({opacity: 1, width: 16, height: 16, left: "-=2", top: "-=2"}, 300);
	$('#solvedvink').animate({opacity: 0, width: 12, height: 12, left: "+=2", top: "+=2"}, 200);
	if (Solved % 2 == 0){
		$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightBlue.jpg" width="100%">')
		$('#donevinkcright').css('cursor', 'pointer');
	}
	else {
		if (doneCRight % 2 == 0){
			$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightRed.jpg" width="100%">')
			$('#donevinkcright').css('cursor', 'no-drop');
			doneCRight--
		}
		else {
			$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightRed.jpg" width="100%">')
			$('#donevinkcright').css('cursor', 'no-drop');
		};
	};
});		

$('#donevinkcright').click(function() {				
    doneCRight++
	$('#donevinkcright').animate({opacity: 1, width: 16, height: 16, left: "-=2", top: "-=2"}, 300);
	$('#donevinkcright').animate({opacity: 0, width: 12, height: 12, left: "+=2", top: "+=2"}, 200);
	if (doneCRight % 2 == 0 && Solved % 2 == 0){
		$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightGreen.jpg" width="100%">')
		$('#donevinkcright').css('cursor', 'pointer');
	}
	if (doneCRight % 2 != 0 && Solved % 2 == 0){
		$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightBlue.jpg" width="100%">')
		$('#donevinkcright').css('cursor', 'pointer');
	}
	if (doneCRight % 2 != 0 && Solved % 2 != 0){
		$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightRed.jpg" width="100%">')
		$('#donevinkcright').css('cursor', 'no-drop');
		doneCRight--
	}
	if (doneCRight % 2 == 0 && Solved % 2 != 0){
		$('#crightimg').replaceWith('<img id="crightimg" src="images/CanteenCandyRightRed.jpg" width="100%">')
		$('#donevinkcright').css('cursor', 'no-drop');
		doneCRight--

	}
});	

/////////////////////////////////////////
////	harmonie PLANOGRAM FUNCTIES	/////
/////////////////////////////////////////

$('#cleftbutton').click(function() {
    $('#cleftbutton').animate({opacity: 1, width: 91, height: 42, left: "-=7.5", top: "-=3.5"}, 300);
	$('#cleftbutton').animate({opacity: 0, width: 76, height: 35, left: "+=7.5", top: "+=3.5"}, 200);
	
	whichPlano='<img src="images/harmonieplanoleftrood.png" width="100%">';			//deze var houdt bij welk plaatje nu actief is (de knop van de juiste planogram moet rood zijn)
				
	if (whichDrawer!=0){
				$('#twoplanograms').fadeIn(1000);
				$('#vulaantalnavi').fadeOut(1000);
				retractDrawers();							// als whichdrawer niet 0 is, betekent dat dat we in een vulaantallen scherm zitten en dus dat een drawer uitgeklapt is. retractDrawers, maakt de juiste drawer weer klein.
				slidePlanogram(1000);			// als dat zo is, moet de planogram pas gaan sliden, nadat de drawer verkleining animatie klaar is
			}
	else {
				slidePlanogram(0);							// als er geen drawer open staat, moet de planogram direct gaan sliden, dus delay 0.
	};
});

$('#crightbutton').click(function() {
    $('#crightbutton').animate({opacity: 1, width: 91, height: 42, left: "-=7.5", top: "-=3.5"}, 300);
	$('#crightbutton').animate({opacity: 0, width: 76, height: 35, left: "+=7.5", top: "+=3.5"}, 200);
	
	whichPlano='<img src="images/harmonieplanorightrood.png" width="100%">';
				
	if (whichDrawer!=0){
				$('#twoplanograms').fadeIn(1000);
				$('#vulaantalnavi').fadeOut(1000);
				retractDrawers();
				slidePlanogram(1000);
			}
	else {
				slidePlanogram(0);
	};
});

function slidePlanogram(delay){						//deze functie wordt gecalled wanneer een planogram knop (links of rechts) wordt geklikt
		setTimeout(function(){						//de timeout delay is alleen nodig als er een la uitgeklapt is (alleen dan wordt een delay meegegeven)

			if (whichPlano=='<img src="images/harmonieplanoleftrood.png" width="100%">'){		//als whichPlano leftrood is 
			$('#draggable').animate({left: 0}, 750);											//dan slide de figuur naar rechts/naar de linker planogram: candy left.
			$('#PhoneImg').html(whichPlano);													//de image veranderd zo dat de juiste planogram knop rood is, in dit geval 'candy left' 
			}	
			else {
			$('#draggable').animate({left: -279}, 750);
			$('#PhoneImg').html(whichPlano);
			};
			
		}, delay);
};

function retractDrawers(){		
	if (whichDrawer==1){
				$('#drawer1').css('opacity', '1');
				$('#drawer1').animate({width: 225, height: 45}, 1000);
				$('#drawer1').animate({opacity: 0}, 300);
				whichDrawer=0;

			};
	if (whichDrawer==2){
				$('#drawer2').css('opacity', '1');
				$('#drawer2').animate({width: 225, height: 45, top: "+=70"}, 1000);
				$('#drawer2').animate({opacity: 0}, 300);
				whichDrawer=0;
			};
			
			$('#testdiv').html('Page 7: Planograms 	<div id="testdivtext">The main planogram is in a visual format, making it very easy for the operator to see which product goes in which spiral at a glance. <BR><BR>The listed planogram is below the graphical one, with only the relevant information for the operator: spiral number, product, price.<BR><BR>Clicking the buttons will navigate to the selected machine&#39;s planogram. <i>Only the top two buttons work in this demo</i>. Horizontally swiping the planogram will also switch between machines. <BR><BR>Each drawer in the visual planogram is clickable, making the filling count input fields of the spirals in that drawer appear. <i>Only the top two drawers work in this demo.</i><BR>This too, makes the work process of the operator more flexible. The current app urges the operator to fill the machine in the order of the listed planogram, making him need to unstack his cart to get to the right products, creating chaos in the process and needing to find put-away products later on. By enabling visual planograms with clickable drawers, the operator can now easily choose the order in which he wants to fill the machine, usually depending on which products are on the top of his cart. </div>');
	};

	function dragy(){		
	$("#draggabley").draggable({ axis: "y", containment: "#containment-wrappery", scroll: false,  				// de id:draggabley kan alleen over de y-as worden gesleept, en alleen binnen de kaders van containment-wrappery
		        drag: function(){
					var offset = $(this).offset();					//de positie van draggabley wordt in deze var opgeslagen wanneer hij gesleept wordt
					var yPos = offset.top;							//de afstand van de top wordt in deze var opgeslagen bij elke sleepbeweging
					
				},
				
		});
	};
	
	
$('#drawer1').click(function() {
	whichDrawer=1;
    $('#drawer1').animate({opacity: 1, height: 275}, 1000);			//drawer1 div vergroot/klapt uit
	$('#drawer1').delay(1000).css('opacity', '0');					//de div wordt na animatie onzichtbaar
	
	$('#twoplanograms').delay(200).fadeOut(1000);					//de planograms verdwijnen
	$('#vulaantalnavi').delay(200).fadeIn(1000);					//en de la met vulaantallen verschijnt
	
	
			var rect = draggabley.getBoundingClientRect();			//van draggabley wordt de locatie opgeslagen (x en y coordinaten)
			$('#testdiv').html('Page 7a: Planograms drawers	<div id="testdivtext">Here, the operator registers how many products he filled per spiral. <BR><BR>Clicking the arrows at the bottom of the drawer will jump to the previous or next drawer. Also dragging the drawer vertically makes it very easy to navigate between drawers. Finally, using the &#39;next&#39; button of the smartphone&#39;s keyboard, the operator can jump to next input fields. <BR><BR>Clicking the planogram icon returns to the visual planogram of the current machine.</div>');
			$('#draggabley').delay(500).animate({top: 0}, 750);		//na 500ms animeert de la vulaantallen naar de eerste la (Als ie daar nog niet was)
			
	dragy();														
});	
	
$('#drawer2').click(function() {
	whichDrawer=2;
    $('#drawer2').animate({opacity: 1, height: 275, top: "-=70"}, 1000);
	$('#drawer2').delay(1000).css('opacity', '0');
	
	$('#twoplanograms').delay(200).fadeOut(1000);
	$('#vulaantalnavi').delay(200).fadeIn(1000);

	
			var rect = draggabley.getBoundingClientRect();
			$('#testdiv').html('Page 7a: Planograms drawers	<div id="testdivtext">Here, the operator registers how many products he filled per spiral. <BR><BR>Clicking the arrows at the bottom of the drawer will jump to the previous or next drawer. Also dragging the drawer vertically makes it very easy to navigate between drawers. Finally, using the &#39;next&#39; button of the smartphone&#39;s keyboard, the operator can jump to next input fields. <BR><BR>Clicking the planogram icon returns to the visual planogram of the current machine.</div>');
			$('#draggabley').delay(500).animate({top: -200}, 750);

		dragy();
});	
	

$('#naarindeling').click(function() {
    $('#naarindeling').animate({opacity: 1, width: 51, height: 37, left: "-=4", top: "-=3"}, 300);
	$('#naarindeling').animate({opacity: 0, width: 43, height: 31, left: "+=4", top: "+=3"}, 200);
	
	if (whichDrawer == 1){
	$('#drawer1').css('opacity', '1');
	$('#drawer1').animate({width: 225, height: 45}, 1000);
	$('#drawer1').animate({opacity: 0}, 300);
	drawer1BigSmall='small';
	}
	else {
	$('#drawer2').css('opacity', '1');
	$('#drawer2').animate({width: 225, height: 45, top: "+=70"}, 1000);
	$('#drawer2').animate({opacity: 0}, 300);
	drawer2BigSmall='small';
	};
	whichDrawer=0;
	$('#twoplanograms').delay(200).fadeIn(1000);
	$('#vulaantalnavi').delay(200).fadeOut(1000);
	
	$('#testdiv').html('Page 7: Planograms 	<div id="testdivtext">The main planogram is in a visual format, making it very easy for the operator to see which product goes in which spiral at a glance. <BR><BR>The listed planogram is below the graphical one, with only the relevant information for the operator: spiral number, product, price.<BR><BR>Clicking the buttons will navigate to the selected machine&#39;s planogram. <i>Only the top two buttons work in this demo</i>. Horizontally swiping the planogram will also switch between machines. <BR><BR>Each drawer in the visual planogram is clickable, making the filling count input fields of the spirals in that drawer appear. <i>Only the top two drawers work in this demo.</i><BR>This too, makes the work process of the operator more flexible. The current app urges the operator to fill the machine in the order of the listed planogram, making him need to unstack his cart to get to the right products, creating chaos in the process and needing to find put-away products later on. By enabling visual planograms with clickable drawers, the operator can now easily choose the order in which he wants to fill the machine, usually depending on which products are on the top of his cart. </div>');
});	

$('#volgendela').click(function() {
    $('#volgendela').animate({opacity: 1, width: 51, height: 37, left: "-=4", top: "-=3"}, 300);
	$('#volgendela').animate({opacity: 0, width: 43, height: 31, left: "+=4", top: "+=3"}, 200);
	
		var rect = draggabley.getBoundingClientRect();
			$('#draggabley').delay(500).animate({top: -200}, 750);
});		

$('#vorigela').click(function() {
    $('#vorigela').animate({opacity: 1, width: 51, height: 37, left: "-=4", top: "-=3"}, 300);
	$('#vorigela').animate({opacity: 0, width: 43, height: 31, left: "+=4", top: "+=3"}, 200);
	
		var rect = draggabley.getBoundingClientRect();
			$('#draggabley').delay(500).animate({top: 0}, 750);
});	




$('#back2').click(function() {
	$('#back2').animate({opacity: 1, width: 52, height: 36, left: "-=4.5", top: "-=3"}, 300);
	$('#back2').animate({opacity: 0, width: 43, height: 30, left: "+=4.5", top: "+=3"}, 200);
	 screenArray.splice(screenArray.length-1, 1);
	 

	 if (currentScreen == "home"){
		 jobsclick();
	 };


	if (currentScreen == "notepage"){
		$('#notepage').fadeOut(500);
		harmonieclick(); 
	};
});




});