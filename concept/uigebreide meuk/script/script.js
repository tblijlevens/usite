$(document).ready(function(){

				
// /main interactions


        $('#sun').hide();
		$('#zontekst').hide();
        $('#earth').hide();
        $('#moon').hide();
		$('#mario').hide();
		$('#mariob').hide()
		$('#marioc').hide()
		$('#mariod').hide()
		$('#marioe').hide()
		$('#mariof').hide()
		$('#mariog').hide()		
		$('#maindiv').animate({left: '+=50%'}, 2000);
		$('#firstdiv').click(function(){
			$('#subdiva').toggle();
			$('#subdiva').animate({left: '+=200px', width:'125px'}, 1500);
			$('#subdiva').css({'cursor':'pointer'});
			$('#subdivb').toggle();
			$('#subdivb').animate({top: '+=200px', width:'125px'}, 1500);
			$('#subdivb').css({'cursor':'pointer'});
			$('#subdivc').toggle();
			$('#subdivc').animate({left: '-=200px', width:'125px'}, 1500);
			$('#subdivc').css({'cursor':'pointer'});
			$('#subdivd').toggle();
			$('#subdivd').animate({top: '-=200px', width:'125px'}, 1500);
			$('#subdivd').css({'cursor':'pointer'});
			$('.text').html('');
			$('#firstdiv').attr("id","nofirstdiv");
		});
		

				
// /Earth orbit interactions


		$('#subdiva').click(function(){
			$('#earth-orbit').show(0);
			$('#subdiva').animate({left: '-=1250px', top: '-=750px', width:'2000px', height:'1500'}, 3000);
			$('#subdiva').toggle('puff', 2000);
			$('#subdivb').toggle('puff', 2000);
			$('#subdivc').toggle('puff', 2000);
			$('#subdivd').toggle('puff', 2000);
			$('#sun').delay(3000).show(3000);
			$('#zontekst').delay(3000).show(3000);
			$('#earth').delay(3000).show(3000);
			$('#moon').delay(3000).show(3000);
			$('#earth-orbit').css({'cursor':'pointer'});
			});
		$('#earth-orbit').click(function(){			
			$('#sun').hide(2000);
			$('#zontekst').hide(2000);
			$('#earth').hide(2000);
			$('#moon').hide(2000);
			$('#subdiva').delay(3000).toggle('puff', 2000);
			$('#subdivb').delay(3000).toggle('puff', 2000);
			$('#subdivc').delay(3000).toggle('puff', 2000);
			$('#subdivd').delay(3000).toggle('puff', 2000);
			$('#subdiva').animate({left: '+=1250px', top: '+=750px', width:'125px', height:'50'}, 3000);
			$('#earth-orbit').delay(3000).hide(1000);
			});
			
			
				
// /Mario interactions


			
		$('#subdivb').click(function(){
			$('#subdivb').css({'z-index':'10'});
			$('#subdivb').animate({left: '-=1050px', top: '-=950px', width:'2000px', height:'1500'}, 3000);
			$('#subdiva').toggle('puff', 2000);
			$('#subdivb').toggle('puff', 2000);
			$('#subdivc').toggle('puff', 2000);
			$('#subdivd').toggle('puff', 2000);
			$('#mario').delay(1000).show(2000);	
			$('#mariob').delay(1500).show(2000);
			$('#marioc').delay(1500).show(2000);	
			$('#mariod').delay(1500).show(2000);
			$('#marioe').delay(1500).show(2000);
			$('#mariof').delay(1500).show(2000);
			$('#mariog').delay(1500).show(2000);
			$('#mario').delay(1500).animate({top: '+=150px'}, 750);
			


			
			$(document).keydown(function(key) {
        switch(parseInt(key.which,10)) {
			// A key pressed
			case 65:
				$('#mario').html("<img src='marioleft.jpg'/> ");		// plaatje veranderd: mario's neus naar links
				
				if ($('#mario').offset().left > 800 & $('#mario').offset().top < 525){ 			// rechts op plateau
					$('#mario').animate({left: '-=30px'}, 100);
				}
				else if ($('#mario').offset().left > 760 & $('#mario').offset().top < 525){ 	//  val van plateau op grond
					$('#mario').animate({left: '-=30px', top: '+=5px'}, 100);
					$('#mario').animate({top: '+=75px'}, 150);
				}
				else if ($('#mario').offset().left > 485 & $('#mario').offset().top > 525){		// op de grond
					$('#mario').animate({left: '-=30px'}, 100);
				}
				else if ($('#mario').offset().left > 300 & $('#mario').offset().top < 525){		// links op plateau
					$('#mario').animate({left: '-=30px'}, 100);
				}
				break;
				

			// D Pressed
			case 68:
				// Put our code here
				$('#mario').html("<img src='mario.jpg'/>"); // plaatje veranderd: mario's neus naar rechts
				
				if ($('#mario').offset().left < 430 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=30px'}, 100);
										}
				else if ($('#mario').offset().left < 470 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=30px', top: '+=5px'}, 100);
					$('#mario').animate({top: '+=75px'}, 150);
					}
				else if ($('#mario').offset().left < 750 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=30px'}, 200);
					}
				else if ($('#mario').offset().left < 920 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=30px'}, 200);
					}
				break;
			
				
			// Spacebar Pressed
			case 32:
				// Put our code here
			
				if($("#mario:contains(' ')").length) { 										// als mario's neus naar links dan springen we naar links
					if ($('#mario').offset().left > 900 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=50'}, 200);	
					}
					else if ($('#mario').offset().left > 770 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=180px', left: '-=50'}, 200);	
					}
					else if ($('#mario').offset().left > 600 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=50'}, 200);	
					}
					else if ($('#mario').offset().left > 580 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=40'}, 200);	
					}
					else if ($('#mario').offset().left > 570 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=30'}, 200);	
					}
					else if ($('#mario').offset().left > 560 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=20'}, 200);	
					}
					else if ($('#mario').offset().left > 550 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=10'}, 200);	
					}
					else if ($('#mario').offset().left > 540 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 460 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=20px', left: '-=10'}, 50);	
					}
					else if($('#mario').offset().left > 425 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '-=50'}, 200);	
					}
					else if ($('#mario').offset().left > 355 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({left: '-=30'}, 50); 
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 325 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=50'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 315 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=40'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 305 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=30'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 295 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=20'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 285 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '-=10'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
					else if ($('#mario').offset().left > 275 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				}
				
				else if ($('#mario').offset().left < 320 & $('#mario').offset().top < 525){		// als  mario's neus niet naar links, dan springen we naar rechts
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=50'}, 200);	
					}
				else if ($('#mario').offset().left < 435 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=180px', left: '+=50'}, 200);	
					}
				else if ($('#mario').offset().left < 640 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=50'}, 200);	
					}
					else if ($('#mario').offset().left < 650 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=40'}, 200);	
					}
					else if ($('#mario').offset().left < 660 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=30'}, 200);	
					}
					else if ($('#mario').offset().left < 670 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=20'}, 200);	
					}
					else if ($('#mario').offset().left < 680 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=10'}, 200);	
					}
					else if ($('#mario').offset().left < 690 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 780 & $('#mario').offset().top > 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=20px', left: '+=10'}, 50);	
					}
				else if ($('#mario').offset().left < 800 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px', left: '+=50'}, 200);	
					}
				else if ($('#mario').offset().left < 870 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({left: '+=30'}, 50); 
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 900 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=50'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 910 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=40'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 920 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=30'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 930 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=20'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 940 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px', left: '+=10'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				else if ($('#mario').offset().left < 950 & $('#mario').offset().top < 525){
					$('#mario').animate({top: '-=100px'}, 200);
					$('#mario').animate({top: '+=100px'}, 200);	
					}
				break;
				
			// E Pressed
			case 69:
				// Put our code here
				$('#mario').html("<img src='mario.jpg'/>");				// plaatje veranderd: mario's neus naar rechts
				if ($('#mario').offset().left < 360 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=100'}, 60);
					}
				else if ($('#mario').offset().left < 460 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=100'}, 60);
					$('#mario').animate({top: '+=80'}, 150);
					}
				else if ($('#mario').offset().left < 670 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=100'}, 60);
					}
				else if ($('#mario').offset().left < 680 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=90'}, 55);
					}
				else if ($('#mario').offset().left < 690 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=80'}, 50);
					}
				else if ($('#mario').offset().left < 700 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=70'}, 45);
					}
				else if ($('#mario').offset().left < 710 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=60'}, 40);
					}
				else if ($('#mario').offset().left < 720 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=50'}, 35);
					}
				else if ($('#mario').offset().left < 730 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=40'}, 30);
					}
				else if ($('#mario').offset().left < 740 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=30'}, 25);
					}
				else if ($('#mario').offset().left < 750 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=20'}, 20);
					}
				else if ($('#mario').offset().left < 760 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '+=10'}, 15);
					}
				else if ($('#mario').offset().left < 760 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=100'}, 60);
					}
				else if ($('#mario').offset().left < 850 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=100'}, 60);
					}
				else if ($('#mario').offset().left < 860 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=90'}, 55);
					}
				else if ($('#mario').offset().left < 870 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=80'}, 50);
					}
				else if ($('#mario').offset().left < 880 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=70'}, 45);
					}
				else if ($('#mario').offset().left < 890 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=60'}, 40);
					}
				else if ($('#mario').offset().left < 900 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=50'}, 35);
					}
				else if ($('#mario').offset().left < 910 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=40'}, 30);
					}
				else if ($('#mario').offset().left < 920 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=30'}, 25);
					}
				else if ($('#mario').offset().left < 930 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=20'}, 20);
					}
				else if ($('#mario').offset().left < 940 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '+=10'}, 15);
					}

				break;
				
			// Q Pressed
			case 81:
				// Put our code here
				$('#mario').html("<img src='marioleft.jpg'/> ");		// plaatje veranderd: mario's neus naar links
				if ($('#mario').offset().left > 860 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=100'}, 60);
					}
				else if ($('#mario').offset().left > 760 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=100'}, 60);
					$('#mario').animate({top: '+=80'}, 150);
					}
				else if ($('#mario').offset().left > 555 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=100'}, 60);
					}
				else if ($('#mario').offset().left > 545 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=90'}, 55);
					}
				else if ($('#mario').offset().left > 535 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=80'}, 50);
					}
				else if ($('#mario').offset().left > 525 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=70'}, 45);
					}
				else if ($('#mario').offset().left > 515 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=60'}, 40);
					}
				else if ($('#mario').offset().left > 505 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=50'}, 35);
					}
				else if ($('#mario').offset().left > 495 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=40'}, 30);
					}
				else if ($('#mario').offset().left > 485 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=30'}, 25);
					}
				else if ($('#mario').offset().left > 475 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=20'}, 20);
					}
				else if ($('#mario').offset().left > 465 & $('#mario').offset().top > 525){
					$('#mario').animate({left: '-=10'}, 15);
					}
				else if ($('#mario').offset().left > 465 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=100'}, 60);
					}
				else if ($('#mario').offset().left > 375 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=100'}, 60);
					}
				else if ($('#mario').offset().left > 365 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=90'}, 55);
					}
				else if ($('#mario').offset().left > 355 & $('#mario').offset().top <525){
					$('#mario').animate({left: '-=80'}, 50);
					}
				else if ($('#mario').offset().left > 345 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=70'}, 45);
					}
				else if ($('#mario').offset().left > 335 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=60'}, 40);
					}
				else if ($('#mario').offset().left > 325 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=50'}, 35);
					}
				else if ($('#mario').offset().left > 315 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=40'}, 30);
					}
				else if ($('#mario').offset().left > 305 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=30'}, 25);
					}
				else if ($('#mario').offset().left > 295 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=20'}, 20);
					}
				else if ($('#mario').offset().left > 285 & $('#mario').offset().top < 525){
					$('#mario').animate({left: '-=10'}, 15);
					}
				break;				
		}
		});
		});
		
		$('#mario').click(function(){
			$('#mario').hide(2000);
			$('#mariob').hide(2000);
			$('#marioc').hide(2000)
			$('#mariod').hide(2000)
			$('#marioe').hide(2000)
			$('#mariof').hide(2000)
			$('#mariog').hide(2000)
			$('#subdiva').delay(3000).toggle('puff', 2000);
			$('#subdivb').delay(3000).toggle('puff', 2000);
			$('#subdivc').delay(3000).toggle('puff', 2000);
			$('#subdivd').delay(3000).toggle('puff', 2000);
			$('#subdivb').animate({left: '+=1050px', top: '+=950px', width:'125px', height:'50'}, 3000);
			$('#subdivb').delay(13000).css({'z-index':'3'});
			});
	
	
				
// /To do list interactions


		
		
		
				
// /Surprise interactions


})

