/*
Author & productowner: Teun Blijlevens, Umanise.
Date:
Contact: mail@umanise.nl
*/


/////////////////////////////////////////////////////////////////////////////
/////////////////////       SET MENU MARKER               ///////////////////
/////////////////////////////////////////////////////////////////////////////

var active_Menu_Item = function() {   // get the name of the page from the url
  var active_Menu_Item = window.location.href.split("/");   // split the url with '/', splitting domainname form page
  active_Menu_Item = active_Menu_Item[active_Menu_Item.length-1]; //get the last item of the array (pagename plus extention)
  active_Menu_Item = active_Menu_Item.split(".");   // split with'.' splitting name from extention'
  active_Menu_Item = active_Menu_Item[0]; // get the name
  return active_Menu_Item;
};
window.onload = function() {

  $("#"+active_Menu_Item()).css({'text-decoration': 'underline', 'background-color': 'rgba(0, 0, 153, 0.1)'}); //detect page and underline that div (menu div has same name as page name)
};


/////////////////////////////////////////////////////////////////////////////
/////////////////////      FILTER FUNCTIONS (CHECKBOXES)         ////////////
/////////////////////////////////////////////////////////////////////////////
function checkboxOnOff (offoron, divid){
    if (offoron == true) { // checkbox is checked
      $("."+divid).css('display', 'inline-block');  // show all the right boxes
    }
    else {    // checkbox is unchecked
      $("."+divid).css('display', 'none');  // get rid of all right boxes
    }
};



/////////////////////////////////////////////////////////////////////////////
/////////////////////       EXPAND BOXES                  ///////////////////
/////////////////////////////////////////////////////////////////////////////
var expanded = [];
function expand(whatWasClicked, sum){
  if (active_Menu_Item()=="diensten"){

    if (sum==1000 || sum == 1300){ // if the div was clicked in the summary or other page wait untill scolled to anchor until expanding
      expanded.push(whatWasClicked);
        jQuery("#"+whatWasClicked).find(".hidden").delay(sum).queue(function (next) {
            $(this).css({'max-height': '800px', transition: 'max-height 1s ease-in'});
            //smoothly resize image only if box is contracted
            if (jQuery("#"+whatWasClicked).find(".hidden").css('max-height') == '140px'){   //if box is contracted
                var elementWidth = jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").width()*1.5; // multiply image width by 1.5
                var elementHeight = jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").height()*1.5;
                jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").animate({width: elementWidth, height: elementHeight}, 500); //resize picture with new values
              }
            next();
          });
        jQuery("#"+whatWasClicked).find('.leesmeer').html("&#9195; Lees minder &#9195;");   //change text


    }
    else {  //if the leesmeer was clicked at the box itself expand immediately
      if (jQuery("#"+whatWasClicked).find(".hidden").css('max-height') == '140px'){   //if box is contracted
        expanded.push(whatWasClicked);

        jQuery("#"+whatWasClicked).find(".hidden").css({'max-height': '800px', transition: 'max-height 1s ease-in'}); //expand smoothly
        jQuery("#"+whatWasClicked).find('.leesmeer').html("&#9195; Lees minder &#9195;");   //change text

    //smoothly resize image
        var elementWidth = jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").width()*1.5; // multiply image width by 1.5
        var elementHeight = jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").height()*1.5;
        jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").animate({width: elementWidth, height: elementHeight}, 500); //resize picture with new values
      }
    else {  // if box is expanded do the opposite
      jQuery("#"+whatWasClicked).find(".hidden").css({'max-height': '140px', transition: 'max-height 0.4s ease-out', overflow: 'hidden'});
      jQuery("#"+whatWasClicked).find('.leesmeer').html("&#9196; Lees meer &#9196;");

      var index = expanded.indexOf(whatWasClicked);
      expanded.splice(index, 1);    // remove divid from array

      elementWidth = jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").width()/1.5;
      elementHeight = jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").height()/1.5;
      jQuery("#"+whatWasClicked).find("#"+active_Menu_Item()+"img").animate({width: elementWidth, height: elementHeight}, 300);
    }

    }

}  //END diensten expand actions

if (active_Menu_Item()=="portfolio"){

  if (jQuery("#"+whatWasClicked).find(".hidden2").css('max-height') == '275px'){   //if box is contracted
    expanded.push(whatWasClicked);  // push divid in array (keeping track of what is expanded)

    jQuery("#"+whatWasClicked).find(".hidden2").css({'max-height': '2200px', transition: 'max-height 1s ease-in'}); //expand smoothly
    jQuery("#"+whatWasClicked).find('.leesmeer').html('&#9195; Lees minder &#9195');   //change text
    jQuery("#"+whatWasClicked).find('.leesmeer').attr('onclick', 'expand("'+whatWasClicked+'"); smoothAnchor("'+whatWasClicked+'");');   //change attribute
//    jQuery("#"+whatWasClicked).find('.leesmeer').html("<span onclick='smoothAnchor("+whatWasClicked+")'>&#9195; Lees minder &#9195;</span>");   //change text
  }
else {  // if box is expanded do the opposite
  jQuery("#"+whatWasClicked).find(".hidden2").css({'max-height': '275px', transition: 'max-height 0.7s ease-out', overflow: 'hidden'});
  jQuery("#"+whatWasClicked).find('.leesmeer').html("&#9196; Lees meer &#9196;");
  jQuery("#"+whatWasClicked).find('.leesmeer').attr('onclick', 'expand("'+whatWasClicked+'")')
  var index = expanded.indexOf(whatWasClicked);
  expanded.splice(index, 1);    // remove divid from array
}
} //END portfolio expand actions
};


///////////////////////////////////////////////////////////////////////////////
// LEES MINDER LINKS GET INPAGE LINK WITH SMOOTH SCROLL TO anchor         /////
///////////////////////////////////////////////////////////////////////////////
function smoothAnchor(div) {    // when a anchor link is clicked (href contains #)
//if link of unchecked category is clicked, it will be checked and all boxes will appear before scrolling to the anchor:
//var divid = $(div).attr('id');
    $('html, body').animate({ scrollTop: $("#"+div).offset().top-75 }, 500);
    return false;
};

/////////////////////////////////////////////////////////////////////////////
/////////////////////       POPUP ON HOVER                  /////////////////
/////////////////////////////////////////////////////////////////////////////
// this function makes the popup appear at the mousecursor
function hoverdiv(e, word, text){
    var left  = e.clientX-2  + "px";  //get cursor position with offset -2 to get popup just under the cursor
    var top  = e.clientY-2  + "px";
    var div = document.getElementById('popup');

    div.style.left = left;  //move popup tot right position
    div.style.top = top;

    $("#popup").html(text + "<br><i>Bron: <a href='https://nl.wikipedia.org/wiki/"+word +"' target='_blank'>Wikipedia</a>.");
    $("#popup").css('display', 'inline');

    return false;
}
function hoverdivout(){
    $("#popup").css('display', 'none');
    return false;
}
